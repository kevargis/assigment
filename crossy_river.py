# import the pygame module
import pygame
import math
import os
import sys
import random
# import pygame.locals for easier
# access to key coordinates
from pygame.locals import *
from configp import *
# import configparser

curr_path = os.path.dirname(__file__)
img_path = os.path.join(curr_path, 'images')
# config_path = os.path.join(curr_path, 'myConfig.cfg')

# configParser = configparser.RawConfigParser()
# configParser.read(config_path)
WIDTH = 800
HEIGHT = 600

# Variable to keep our game loop running
gameOn = True
player1On = True
player2On = False
GRound = 1


class Player(object):
    def __init__(self):
        """ The constructor of the class """
        # the Player's position
        self.x = 0
        self.y = 0
        self.img_b = pygame.image.load(os.path.join(img_path, 'player1.png'))
        self.image = pygame.transform.rotozoom(self.img_b, 0, 0.07)
        self.rect = self.image.get_rect()
        self.lose = 0
        self.score = 0
        self.chk = 0
        self.speed = 1
        self.wins = 0

    def handle_keys1(self):
        """ Handles Keys """
        key = pygame.key.get_pressed()
        dist = 5  # distance moved in 1 frame, try changing it to 5
        if key[pygame.K_DOWN]:  # down key
            self.y += dist  # move down
        elif key[pygame.K_UP]:  # up key
            self.y -= dist  # move up
        if key[pygame.K_RIGHT]:  # right key
            self.x += dist  # move right
        elif key[pygame.K_LEFT]:  # left key
            self.x -= dist  # move left
        if(self.x > (WIDTH - 35)):
            self.x -= dist
        elif(self.x < 0):
            self.x += dist
        elif(self.y < 0):
            self.y += dist
        elif(self.y > (HEIGHT - 35)):
            self.y -= dist
        #hits = pygame.sprite.spritecollide(self.player, self.platforms, False)

    def handle_keys2(self):
        """ Handles Keys """
        key = pygame.key.get_pressed()
        dist = 5  # distance moved in 1 frame, try changing it to 5
        if key[pygame.K_s]:  # down key
            self.y += dist  # move down
        elif key[pygame.K_w]:  # up key
            self.y -= dist  # move up
        if key[pygame.K_d]:  # right key
            self.x += dist  # move right
        elif key[pygame.K_a]:  # left key
            self.x -= dist  # move left
        if(self.x > (WIDTH - 35)):
            self.x -= dist
        elif(self.x < 0):
            self.x += dist
        elif(self.y < 0):
            self.y += dist
        elif(self.y > (HEIGHT - 35)):
            self.y -= dist

    def draw(self, surface):
        """ Draw on surface """
        # blit yourself at your current position
        surface.blit(self.image, (self.x, self.y))

    # def checkCollision(self, sprite1, sprite2):
    #     col = pygame.sprite.collide_rect(sprite1, sprite2)
    #     if col == True:
    #         gameOn = False


obstacles = pygame.sprite.Group()


class Boulder(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """ The constructor of the class """
        pygame.sprite.Sprite.__init__(self, obstacles)
        self.img_b = pygame.image.load(os.path.join(img_path, 'boulder.png'))
        self.image = pygame.transform.rotozoom(self.img_b, 0, 0.06)
        # the Player's position
        self.x = x
        self.y = y
        self.rect = self.image.get_rect()


spd = 1


class Yacht(pygame.sprite.Sprite):
    def __init__(self, x, y):
        """ The constructor of the class """
        pygame.sprite.Sprite.__init__(self, obstacles)
        self.img_b = pygame.image.load(os.path.join(img_path, 'yatch.png'))
        self.image = pygame.transform.rotozoom(self.img_b, 0, 0.2)
        # the Player's position
        self.x = x
        self.y = y
        self.rect = self.image.get_rect()

    def move(self):
        if(self.x > 600):
            self.x = 0
        self.x += spd

# Define our bgRect object for creating partitons and call super to
# give it all the properties and methods of pygame.sprite.Sprite
# Define the class for our bgRect objects


class bgRect(pygame.sprite.Sprite):
    def __init__(self):
        super(bgRect, self).__init__()

        # Define the dimension of the surface
        # Here we are making bgRects of side 25px
        self.surf = pygame.Surface((WIDTH, 30))

        # Define the color of the surface using RGB color coding.
        self.surf.fill((153, 76, 0))
        #self.rect = self.surf.get_rect()


# def game_over_text():
#     over_text = over_font.render("GAME OVER", True, (255, 255, 255))
#     screen.blit(over_text, (200, 250))

def iscollision(enemyx, enemyy, playerx, playery):
    distance = math.sqrt((math.pow(enemyx - playerx, 2)) +
                         (math.pow(enemyy - playery, 2)))
    if(distance < 30):
        return True
    else:
        False


def iscollision1(enemyx, enemyy, playerx, playery):
    distance = math.sqrt((math.pow(enemyx + 30 - playerx, 2)) +
                         (math.pow(enemyy + 30 - playery, 2)))
    if(distance < 50):
        return True
    else:
        False


# initialize pygame
pygame.init()

#pygame.mixer.init(44100, -16,2,2048)


# over_font = pygame.font.Font('freesansbold.ttf', 64)
# score_font = pygame.font.Font('freesansbold.ttf', 30)


# Define the dimensions of screen object
screen = pygame.display.set_mode((WIDTH, HEIGHT))


# instansiate player
player1 = Player()
player1.x = 400     #spawning location
player1.y = 600
player1.img_b = pygame.image.load(os.path.join(img_path, 'player1.png'))    #getting icon
player1.image = pygame.transform.rotozoom(player1.img_b, 0, 0.07)

player2 = Player()
player2.x = 400
player2.y = 0
player2.img_b = pygame.image.load(os.path.join(img_path, 'player2.png'))
player2.image = pygame.transform.rotozoom(player2.img_b, 0, 0.5)
player2.image = pygame.transform.rotate(player2.image, 180)

clock = pygame.time.Clock()

# instantiate all partition objects
bgRect1 = bgRect()
bgRect2 = bgRect()
bgRect3 = bgRect()
bgRect4 = bgRect()
bgRect5 = bgRect()
bgRect6 = bgRect()

# instantiate all Boulder objects
boulder1 = Boulder(30, 110)
boulder2 = Boulder(240, 110)
boulder3 = Boulder(90, 230)
boulder4 = Boulder(360, 230)
boulder5 = Boulder(120, 340)
boulder6 = Boulder(420, 340)
boulder7 = Boulder(50, 460)
boulder8 = Boulder(300, 460)
boulder9 = Boulder(360, 110)
boulder10 = Boulder(720, 110)
boulder11 = Boulder(720, 230)
boulder12 = Boulder(560, 230)
boulder13 = Boulder(630, 340)
boulder14 = Boulder(760, 340)
boulder15 = Boulder(600, 460)
boulder16 = Boulder(690, 460)

#instansiate yachts
yacht1 = Yacht(500, 20)
yacht2 = Yacht(200, 130)
yacht3 = Yacht(350, 250)
yacht4 = Yacht(70, 360)
yacht5 = Yacht(600, 480)


# Our game loop
start_time = pygame.time.get_ticks()
while gameOn:
    # for loop through the event queue
    for event in pygame.event.get():

        # Check for KEYDOWN event
        if event.type == KEYDOWN:

            # If the Backspace key has been pressed set
            # running to false to exit the main loop
            if event.key == K_BACKSPACE:
                score_lab = over_font.render(str(player1.wins), 1, (255, 255, 255))
                screen.blit(w1_text, (200, 200))
                screen.blit(score_lab, (655, 200))
                score_lab = over_font.render(str(player2.wins), 1, (255, 255, 255))
                screen.blit(w2_text, (200, 300))
                screen.blit(score_lab, (655, 300))
                #print("hoi")
                pygame.display.flip()
                pygame.time.wait(2500)
                gameOn = False

        # Check for QUIT event
        elif event.type == QUIT:
            score_lab = over_font.render(str(player1.wins), 1, (255, 255, 255))
            screen.blit(w1_text, (200, 200))
            screen.blit(score_lab, (655, 200))
            score_lab = over_font.render(str(player2.wins), 1, (255, 255, 255))
            screen.blit(w2_text, (200, 300))
            screen.blit(score_lab, (655, 300))
            #print("hoi")
            pygame.display.flip()
            pygame.time.wait(2500)
            gameOn = False

    # Define where the bgRects will appear on the screen
    # Use blit to draw them on the screen surface
    screen.fill((0, 200, 255))
    screen.blit(bgRect1.surf, (0, 0))
    screen.blit(bgRect2.surf, (0, 110))
    screen.blit(bgRect3.surf, (0, 230))
    screen.blit(bgRect4.surf, (0, 340))
    screen.blit(bgRect5.surf, (0, 460))
    screen.blit(bgRect6.surf, (0, 570))

    screen.blit(boulder1.image, (30, 110))
    screen.blit(boulder2.image, (240, 110))
    screen.blit(boulder9.image, (360, 110))
    screen.blit(boulder10.image, (720, 110))
    screen.blit(boulder3.image, (90, 230))
    screen.blit(boulder4.image, (360, 230))
    screen.blit(boulder11.image, (720, 230))
    screen.blit(boulder12.image, (560, 230))
    screen.blit(boulder5.image, (120, 340))
    screen.blit(boulder6.image, (420, 340))
    screen.blit(boulder13.image, (630, 340))
    screen.blit(boulder14.image, (760, 340))
    screen.blit(boulder7.image, (50, 460))
    screen.blit(boulder8.image, (300, 460))
    screen.blit(boulder15.image, (600, 460))
    screen.blit(boulder16.image, (690, 460))

    yacht1.move()
    screen.blit(yacht1.image, (yacht1.x, yacht1.y))
    yacht2.move()
    screen.blit(yacht2.image, (yacht2.x, yacht2.y))
    yacht3.move()
    screen.blit(yacht3.image, (yacht3.x, yacht3.y))
    yacht4.move()
    screen.blit(yacht4.image, (yacht4.x, yacht4.y))
    yacht5.move()
    screen.blit(yacht5.image, (yacht5.x, yacht5.y))

    if player1On:   #player1 loop
        player1.handle_keys1()  #handles key presses
        player1.draw(screen)    #draws the player
        score_lab = score_font.render(str(player1.score), 1, (0, 0, 0))
        screen.blit(score1_text, (0, 0))    #gets and prints the score
        screen.blit(score_lab, (225, 0))
        screen.blit(start_text, (700, 570))
        screen.blit(end_text, (700, 0))
        spd = player1.speed     #get the players speed
        #over_score = over_font.render(player1.score, True, (255, 255, 255))
        #screen.blit(over_score, (0, 0))

        # for enemy in obstacles.sprites():
        #checks for collisions
        collision = iscollision(boulder1.x, boulder1.y, player1.x, player1.y)
        if collision:
            player1On = False   #switching control
            player2On = True
        collision = iscollision(boulder2.x, boulder2.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder3.x, boulder3.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder4.x, boulder4.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder5.x, boulder5.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder6.x, boulder6.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder7.x, boulder7.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder8.x, boulder8.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder9.x, boulder9.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder10.x, boulder10.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder11.x, boulder11.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder12.x, boulder12.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder13.x, boulder13.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder14.x, boulder14.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder15.x, boulder15.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True
        collision = iscollision(boulder16.x, boulder16.y, player1.x, player1.y)
        if collision:
            player1On = False
            player2On = True

        # print(yacht1.x)
        # print(yacht1.y)
        collision = iscollision1(yacht1.x, yacht1.y, player1.x, player1.y)
        if collision:
            player1On = False
        collision = iscollision1(yacht2.x, yacht2.y, player1.x, player1.y)
        if collision:
            player1On = False
        collision = iscollision1(yacht3.x, yacht3.y, player1.x, player1.y)
        if collision:
            player1On = False
        collision = iscollision1(yacht4.x, yacht4.y, player1.x, player1.y)
        if collision:
            player1On = False
        collision = iscollision1(yacht5.x, yacht5.y, player1.x, player1.y)
        if collision:
            player1On = False
        if not player1On:   #after collisison
            player2On = True    #switching control
            player1.lose += 1   #incrementing loss count
            player1.x = 400     #setting respawn
            player1.y = 570
            passed_time = math.floor(   #checks time for scoring
                (pygame.time.get_ticks() - start_time) / 1000)
            # print(passed_time)
            player1.score = player1.score - passed_time
            #print(player1.score)
            #score_text = score_font.render("Time deduction: ", 1, (0, 0, 0))
            score_lab = score_font.render(str(passed_time), 1, (0, 0, 0))
            screen.blit(score_text, (300, 0))   #prints time deduction
            screen.blit(score_lab, (540, 0))
            pygame.display.flip()
            pygame.time.wait(2500)
            start_time = pygame.time.get_ticks()    #reset timer

        # if GRound % 2 == 1:
        if player1.y == 0:      #checks if player reached end
            player1On = False
            player2On = True
            #player1.image = pygame.transform.rotate(player1.image, 180)
            player1.x = 400
            player1.y = 570
            player1.chk = 0
            passed_time = math.floor(
                (pygame.time.get_ticks() - start_time) / 1000)
            # print(passed_time)
            player1.score = player1.score - passed_time
            #print(player1.score)
            #score_text = score_font.render("Time deduction: ", 1, (0, 0, 0))
            score_lab = score_font.render(str(passed_time), 1, (0, 0, 0))
            screen.blit(score_text, (300, 0))
            screen.blit(score_lab, (540, 0))
            pygame.display.flip()
            pygame.time.wait(2500)
            start_time = pygame.time.get_ticks()
        # else:
        #     if player1.y > 560:
        #         player1On = False
        #         player2On = True
        #         player1.image = pygame.transform.rotate(player1.image, 180)
        #         print(player1.score)
        #         player1.x = 400s
        #         player1.y = 570
        if player1.y < 480 and player1.chk == 0:    #increasing score for player as needed
            player1.score += 10
            player1.chk += 1
        elif player1.y < 460 and player1.chk == 1:
            player1.score += 5
            player1.chk += 1
        elif player1.y < 360 and player1.chk == 2:
            player1.score += 10
            player1.chk += 1
        elif player1.y < 340 and player1.chk == 3:
            player1.score += 5
            player1.chk += 1
        elif player1.y < 250 and player1.chk == 4:
            player1.score += 10
            player1.chk += 1
        elif player1.y < 230 and player1.chk == 5:
            player1.score += 5
            player1.chk += 1
        elif player1.y < 130 and player1.chk == 6:
            player1.score += 10
            player1.chk += 1
        elif player1.y < 110 and player1.chk == 7:
            player1.score += 5
            player1.chk += 1
        elif player1.y < 20 and player1.chk == 8:
            player1.score += 10
            player1.chk += 1

    if player2On:   #player2 loop
        player2.handle_keys2()
        player2.draw(screen)
        score_lab = score_font.render(str(player2.score), 1, (0, 0, 0))
        screen.blit(score2_text, (0, 0))
        screen.blit(score_lab, (225, 0))
        screen.blit(start_text, (700, 0))
        screen.blit(end_text, (700, 570))
        spd = player2.speed
        #screen.blit(player2.score, (0, 0))
        # game_over_text()

        # for enemy in obstacles.sprites():
        collision = iscollision(boulder1.x, boulder1.y, player2.x, player2.y)
        if collision:
            player2On = False
            player1On = True

        collision = iscollision(boulder2.x, boulder2.y, player2.x, player2.y)
        if collision:
            player2On = False
            player1On = True
        collision = iscollision(boulder3.x, boulder3.y, player2.x, player2.y)
        if collision:
            player2On = False
            player1On = True
        collision = iscollision(boulder4.x, boulder4.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder5.x, boulder5.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder6.x, boulder6.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder7.x, boulder7.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder8.x, boulder8.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder9.x, boulder9.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder10.x, boulder10.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder11.x, boulder11.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder12.x, boulder12.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder13.x, boulder13.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder14.x, boulder14.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder15.x, boulder15.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision(boulder16.x, boulder16.y, player2.x, player2.y)
        if collision:
            player2On = False

        # print(yacht1.x)
        # print(yacht1.y)
        collision = iscollision1(yacht1.x, yacht1.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision1(yacht2.x, yacht2.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision1(yacht3.x, yacht3.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision1(yacht4.x, yacht4.y, player2.x, player2.y)
        if collision:
            player2On = False
        collision = iscollision1(yacht5.x, yacht5.y, player2.x, player2.y)
        if collision:
            player2On = False
        if not player2On:
            player1On = True
            player2.lose += 1
            player2.x = 400
            player2.y = 0
            passed_time = math.floor(
                (pygame.time.get_ticks() - start_time) / 1000)
            player2.score = player2.score - passed_time
            score_lab = score_font.render(str(passed_time), 1, (0, 0, 0))
            screen.blit(score_text, (300, 0))
            screen.blit(score_lab, (540, 0))
            pygame.display.flip()
            pygame.time.wait(2500)
            #print(player2.score)
            if player2.lose > player1.lose:     #checks for winner
                player1.speed += 1
                player1.wins += 1
                screen.blit(p1_text, (200, 250))
                pygame.display.flip()
                pygame.time.wait(2500)
                #print("1 wins")
            elif player2.lose > player1.lose:
                player2.speed += 1
                player2.wins += 1
                screen.blit(p2_text, (200, 250))
                pygame.display.flip()
                pygame.time.wait(2500)
                #print("2 wins")
            else:
                #print("tie")
                #print(player1.score)
                #print(player2.score)
                if player1.score > player2.score:
                    player1.speed += 1
                    player1.wins += 1
                    screen.blit(p1_text, (200, 250))
                    pygame.display.flip()
                    pygame.time.wait(2500)
                elif player1.score < player2.score:
                    player2.speed += 1
                    player2.wins += 1
                    screen.blit(p2_text, (200, 250))
                    pygame.display.flip()
                    pygame.time.wait(2500)
                else:
                    screen.blit(tie_text, (350, 250))
                    pygame.display.flip()
                    pygame.time.wait(2500)
            player1.score = 0
            player2.score = 0
            player1.lose = 0
            player2.lose = 0
            start_time = pygame.time.get_ticks()

        # if GRound % 2 == 1:
        if player2.y > 560:
            player2On = False
            player1On = True
            #player2.image = pygame.transform.rotate(player2.image, 180)
            GRound += 1
            #print(GRound)
            player2.x = 400
            player2.y = 0
            if player1.lose == 1 and player2.lose == 0:
                #print("2 wins")
                player2.speed += 1
                player2.wins += 1
                screen.blit(p2_text, (200, 250))
                #gameOn = False
            #spd += 1
            player2.chk = 0
            passed_time = math.floor(
                (pygame.time.get_ticks() - start_time) / 1000)
            player2.score = player2.score - passed_time
            #score_text = score_font.render("Time deduction: ", 1, (0, 0, 0))
            score_lab = score_font.render(str(passed_time), 1, (0, 0, 0))
            screen.blit(score_text, (300, 0))
            screen.blit(score_lab, (540, 0))
            pygame.display.flip()
            pygame.time.wait(2500)
            #print(player2.score)
            # if player1.score > player2.score:
            #     over_text = over_font.render("Player 1 Wins", True, (255, 255, 255))
            #     screen.blit(over_text, (200, 250))
            #     pygame.display.flip()
            #     pygame.time.wait(2500)
            # elif player1.score < player2.score:
            #     over_text = over_font.render("Player 2 Wins", True, (255, 255, 255))
            #     screen.blit(over_text, (200, 250))
            #     pygame.display.flip()
            #     pygame.time.wait(2500)
            # else:
            #     over_text = over_font.render("Tie", True, (255, 255, 255))
            #     screen.blit(over_text, (200, 250))
            #     pygame.display.flip()
            #     pygame.time.wait(2500)
            if player2.lose > player1.lose:
                player1.speed += 1
                player1.wins += 1
                screen.blit(p1_text, (200, 250))
                pygame.display.flip()
                pygame.time.wait(2500)
                #print("1 wins")
            elif player2.lose < player1.lose:
                player2.speed += 1
                player2.wins += 1
                screen.blit(p2_text, (200, 250))
                pygame.display.flip()
                pygame.time.wait(2500)
                #print("2 wins")
            else:
                #print(player1.score)
                #print(player2.score)
                if player1.score > player2.score:
                    player1.speed += 1
                    player1.wins += 1
                    screen.blit(p1_text, (200, 250))
                    pygame.display.flip()
                    pygame.time.wait(2500)
                elif player1.score < player2.score:
                    player2.speed += 1
                    player2.wins += 1
                    screen.blit(p2_text, (200, 250))
                    pygame.display.flip()
                    pygame.time.wait(2500)
                else:
                    screen.blit(tie_text, (350, 250))
                    pygame.display.flip()
                    pygame.time.wait(2500)
            player1.score = 0
            player2.score = 0
            player1.lose = 0
            player2.lose = 0
            start_time = pygame.time.get_ticks()

        if player2.y > 480 and player2.chk == 8:
            player2.score += 10
            player2.chk += 1
        elif player2.y > 460 and player2.chk == 7:
            player2.score += 5
            player2.chk += 1
        elif player2.y > 360 and player2.chk == 6:
            player2.score += 10
            player2.chk += 1
        elif player2.y > 340 and player2.chk == 5:
            player2.score += 5
            player2.chk += 1
        elif player2.y > 250 and player2.chk == 4:
            player2.score += 10
            player2.chk += 1
        elif player2.y > 230 and player2.chk == 3:
            player2.score += 5
            player2.chk += 1
        elif player2.y > 130 and player2.chk == 2:
            player2.score += 10
            player2.chk += 1
        elif player2.y > 110 and player2.chk == 1:
            player2.score += 5
            player2.chk += 1
        elif player2.y > 20 and player2.chk == 0:
            player2.score += 10
            player2.chk += 1
        # else:
        #     if player2.y == 0:
        #         player2On = False
        #         player1On = True
        #         player2.image = pygame.transform.rotate(player2.image, 180)
        #         GRound += 1
        #         print(GRound)
        #         player2.x = 400
        #         player2.y = 0
        #         if player1.lose == 1 and player2.lose == 0:
        #             over_text = over_font.render("Player 2 Wins", True, (255, 255, 255))
        #             screen.blit(over_text, (200, 250))
        #             print("2 wins")
        #             gameOn = False
        #         spd += 1

    #a = a + 1
    # for enemy in obstacles:
    #     collision = iscollision(enemy.x, enemy.y, player1.x, player1.y)
    #     if collision:
    #         gameOn = False

    # Update the display using flip
    pygame.display.flip()
    clock.tick(40)  #setting fps
